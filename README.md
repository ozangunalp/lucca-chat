A test project for a simple in browser chat application

Go ahead and test the app by:
	* installing necessary node packages by `npm install`
	* installing project dependencies by `bower install`
	* building the project by `grunt build`
	* and at last opening `dist/index.html`


