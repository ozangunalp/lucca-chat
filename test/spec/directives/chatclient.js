'use strict';

describe('Directive: chatclient', function () {

  // load the directive's module
  beforeEach(module('luccaChatApp'));

  var element,
    scope;

  beforeEach(inject(function ($rootScope) {
    scope = $rootScope.$new();
  }));

    it('should make hidden element visible', inject(function ($compile) {
        element = angular.element('<chat client="clientId"></chat>');
        element = $compile(element)(scope);
        expect(element !== undefined).toBe(true);
        expect(element.text()).toBe('');
    }));
});
