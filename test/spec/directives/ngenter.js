'use strict';

describe('Directive: ngenter', function () {

  // load the directive's module
  beforeEach(module('luccaChatApp'));

    var element,
        scope;

    beforeEach(inject(function ($rootScope) {
        scope = $rootScope.$new();
    }));

    it('should make hidden element visible', inject(function ($compile) {
        element = angular.element('<div ng-enter=""></div>');
        element = $compile(element)(scope);
        expect(!!element).toBe(true);
    }));
});
