'use strict';

describe('Service: chat', function () {

  // load the service's module
  beforeEach(module('luccaChatApp'));

  // instantiate service
  var chat;
  beforeEach(inject(function (_$chat_) {
    chat = _$chat_;
  }));

    it('should be injected', function () {
        expect(!!chat).toBe(true);
    });

});
