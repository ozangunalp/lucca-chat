'use strict';

/**
 * @ngdoc overview
 * @name luccaChatApp
 * @description
 * # luccaChatApp
 *
 * Main module of the application.
 */
angular
  .module('luccaChatApp', [
    'ngAnimate',
    'ngResource',
    'ngRoute',
    'luegg.directives',
    'angularMoment',
    'contenteditable'
  ])
  .config(function ($routeProvider) {
    $routeProvider
      .when('/chats/:clientId', {
        templateUrl: 'views/chat.html',
        controller: 'ChatCtrl'
      })
      .otherwise({
        redirectTo: '/'
      });
  });
