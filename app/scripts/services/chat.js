'use strict';

/**
 * @ngdoc service
 * @name luccaChatApp.chat
 * @description
 * # chat
 * Service in the luccaChatApp.
 */
angular.module('luccaChatApp')
    .service('$chat', function () {
        // AngularJS will instantiate a singleton by calling "new" on this function
        var service = {};
        service.messages = [];
        service.callbacks = {};
        service.register = function (id, callback) {
            service.callbacks[id] = callback;
        };
        service.send = function (message) {
            service.messages.push(message);
            for (var i = 0, keys = Object.keys(service.callbacks), j = keys.length; i < j; i++) {
                //if (keys[i] !== message.messageFrom) {
                    service.callbacks[keys[i]](message);
                //}
            }
        };
        return service;
    });
