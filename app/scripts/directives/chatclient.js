'use strict';

/**
 * @ngdoc directive
 * @name luccaChatApp.directive:chatclient
 * @description
 * # chatclient
 */
angular.module('luccaChatApp')
    .directive('chat', [ '$chat', function factory($chat) {
        return {
            restrict: 'E',
            templateUrl: 'views/chat.html',
            scope: {
                'client': '='
            },
            controller: function ($scope) {
                $scope.message = '';
                $scope.messages = [];
                $chat.register($scope.client, function (message) {
                    $scope.messages.push(message);
                });
                $scope.sendMessage = function () {
                    if ($scope.message !== '') {
                        var eventTime = new Date();
                        var m = {
                            'messageText': $scope.message,
                            'messageTimestamp': eventTime,
                            'messageFrom': $scope.client
                        };
                        //$scope.messages.push(m);
                        $chat.send(m);
                        $scope.message = '';
                    }
                };
            }
        };
    }]);
